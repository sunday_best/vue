import { createStore,useStore as baseUseStore } from 'vuex'
import type  {Store} from 'vuex'
import type {UsersState} from './modules/users'
import type { InjectionKey } from 'vue'
// import checks from './modules/checks'
// import news from './modules/news'
import users from './modules/users'
// import signs from './modules/signs'

export interface State{

}

export interface StateAll extends State{
  users:UsersState
}

export const key:InjectionKey<Store<StateAll>> = Symbol() 

export function useStore () {
  return baseUseStore(key)
}

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    
  }
})
