import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
const Login = () => import('@/views/Login/Login.vue');
const Apply = () => import('@/views/Apply/Apply.vue');
const Exception = () => import('@/views/Exception/Exception.vue');
const Check = () => import('@/views/Check/Check.vue');
const Home = () => import('@/views/Home/Home.vue');
const Sign = () => import('@/views/Sign/Sign.vue');

declare module 'vue-router'{
  interface RouteMeta{
    menu?:boolean
    title?:string
    icon?:string
    auth?:boolean
  }
}

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    redirect:'Sign',
    component: Home,
    meta:{
      menu:true,
      title:'考勤管理',
      icon:'Message-Box',
      auth:true
    },
    children:[
      {
        path: 'Apply',
        name: 'Apply',
        component: Apply,
        meta: {
          menu: true,
          title: '添加考勤审批',
          icon: 'document-add',
          auth: true,
        }
      },
      {
        path: 'Exception',
        name: 'Exception',
        meta: {
          menu: true,
          title: '异常考勤查询',
          icon: 'warning',
          auth: true,
        },
        component: Exception,
      },
      {
        path: 'Check',
        name: 'Check',
        component: Check,
        meta: {
          menu: true,
          title: '我的考勤审批',
          icon: 'finished',
          auth: true,
        }
      },
      {
        path: 'Sign',
        name: 'Sign',
        meta: {
          menu: true,
          title: '在线打卡签到',
          icon: 'calendar',
          auth: true
        },
        component: Sign,
      }
    ]
  },
  {
    path: '/Login',
    name: 'Login',
    component:Login
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
